# NLP Chatbot

## Description
Indonesian NLP Chatbot built with Flask and Socket.io. This project requires Python 3.

### Screenshot
![Chatbox UI](chat_box.png)

## How to use it
### Build the model
Use these steps to build question-answer set model
- Install requirements
```shell
pip3 install -r requirements.txt
```
- Prepare the question-answer set in `intents.json` file 
- Build the model
```shell
python3 train_chatbot.py
```

### Run the project
Use these steps to run the project with pre-built model
- Run the app
```shell
flask run
```

## References
qna set model: [Dzone](https://dzone.com/articles/python-chatbot-project-build-your-first-python-pro)

