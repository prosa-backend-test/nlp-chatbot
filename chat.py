# chat.py
import os
import json
import time
import telegram
from redis import StrictRedis
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
from flask.logging import create_logger
from chatbot import getResponse, predict_class

app = Flask(__name__)
log = create_logger(app)
app.debug = 'DEBUG' in os.environ

socketio = SocketIO(app)
global bot
bot = telegram.Bot(token=os.getenv('telebot_token'))
redis = StrictRedis(host='localhost', port=6379)

def event_handler(message):
    socketio.emit('dialog', {'data': 'Sepertinya kamu tidak merespon :( Daaah', 'type': 'bot', 'timestamp': time.time()})
    thread.stop()

pubsub = redis.pubsub()
pubsub.psubscribe(**{'__keyevent@0__:expired': event_handler})

thread = pubsub.run_in_thread(sleep_time=0.01)

@app.route('/')
def hello():
    return render_template('chat.html')

@app.route('/setWebhook', methods=['GET', 'POST'])
def set_webhook():
    ok = bot.set_webhook(url='{URL}{TOKEN}'.format(os.getenv('url'), os.getenv('telebot_token')))
    if ok:
        return "webhook setup ok"
    else:
        return "webhook setup failed"

@app.route('/{}'.format(os.getenv('telebot_token')), methods=['POST'])
def respond():
    update = telegram.Update.de_json(request.get_json(force=True), bot)
    chat_id = update.message.chat.id
    msg_id = update.message.chat.message_id
    text = update.message.text.encode('utf-8').decode()
    
    ints = predict_class(text)
    intents = json.loads(open('intents.json').read())
    response = getResponse(ints, intents)

    bot.send_message(chat_id=chat_id, text=response, reply_to_message_id=msg_id)

@socketio.on('connect')
def handle_chat():
    emit('chat', {'data':'You\'re connected to our bot'})

@socketio.on('dialog')
def handle_response_dialog(message):
    ints = predict_class(message['data'])
    intents = json.loads(open('intents.json').read())
    res = getResponse(ints, intents)
    emit('dialog', {'data': message['data'], 'type': 'human', 'timestamp': time.time()})
    emit('dialog', {'data': res, 'type': 'bot', 'timestamp': time.time()})
    redis.set(name=request.sid, value=request.sid, ex=60)

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)