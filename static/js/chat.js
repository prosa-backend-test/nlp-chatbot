const $events = document.getElementById('events');
const $dialog = document.getElementById('messages');

const monthName = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

const newItem = (content) => {
    const item = document.createElement('li');
    const sender = document.createElement('p');
    const message = document.createElement('p');
    let humanTime = new Date(content['timestamp'] * 1000);
    const date = String(humanTime.getDate()).padStart(2, '0');
    const month = humanTime.getMonth();
    const year = humanTime.getFullYear();
    const hour = String(humanTime.getHours()).padStart(2, '0');
    const min = String(humanTime.getMinutes()).padStart(2, '0');
    const sec = String(humanTime.getSeconds()).padStart(2, '0');
    const timestamp = ' pada ' + date + ' ' + monthName[month-1] + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    sender.innerText = content['type'] == 'human'  ? 'Anda'+ timestamp : 'Bot' + timestamp;
    sender.className = 'sender';
    message.innerText = content['data'];
    item.className = 'message';
    content['type'] == 'human' ? item.classList.add('human-message') : item.classList.add('bot-message');
    item.appendChild(sender);
    item.appendChild(message);
    return item;
}

const newItemFromSocketStatus = (content) => {
    const item = document.createElement('li');
    item.className = 'socket-status';
    item.innerText = content['data'];
    return item;
}

const socket = io();

socket.on('connect', () => {
    socket.on('chat', (message) => {
        $dialog.appendChild(newItemFromSocketStatus(message))
    });
});

$('form#chat-box').submit(function(event) {
    socket.emit('dialog', {data: $('#chat-text').val(), type:'human'});
    return false;
});

socket.on('dialog', function(message){
    $dialog.appendChild(newItem(message))
    let form = document.getElementById("chat-box");
    form.reset();
    $("#chat-text").focus()
    scrollThrough();
});

function scrollThrough() {
    $dialog.scrollTop = $dialog.scrollHeight
}